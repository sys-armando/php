<?php

require('frog.php');
require('ape.php');

$obj = new Animal("Shaun");

echo "Name : " . $obj->name . "<br>";
echo "Legs : " . $obj->legs . "<br>";
echo "Cold Blooded : " . $obj->cold_blooded . "<br><br>";

$obj2 = new frog("Frog");
echo "Name  : " . $obj2->name . "<br>";
echo "Legs : " . $obj2->legs . "<br>";
echo "Cold Blooded : " . $obj->cold_blooded . "<br>";
$obj2->jump();
echo "<br><br>";

$obj3 = new ape("Ape");
echo "Name : " . $obj3->name . "<br>";
echo "Legs : " . $obj3->legs . "<br>";
echo "Cold Blooded : " . $obj->cold_blooded . "<br>";
$obj3->yell();
