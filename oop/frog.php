<?php
require('animal.php');

class frog extends Animal
{
    public $legs = 4;

    public function jump()
    {
        echo "Jump : Hop Hop";
    }
}
